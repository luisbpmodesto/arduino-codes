#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>



#define UBRRVAL F_CPU/8/BAUD-1 /* calculates UBRRVAL for BAUD baudrate, BAUD must be defined */


#define buffer_size  128
char rxbuffer[buffer_size];
volatile int commandflag  = 0;
int readflag = 0;
int readpos =0;
float dutycicle = 50.0;
int NEWTOP=64000;
int val;


static int uputc(char,FILE*);
void validatecommand();
void commands();
void timer1config();
void auxtimer();


static int uputc(char c,FILE *stream)
/* uart writer function for libc stdio functions */
{
	if (c == '\n')
		uputc('\r',stream);
	loop_until_bit_is_set(UCSR0A, UDRE0);	/* wait until we can send a new byte */
	UDR0 = (uint8_t) c;
	return 0;
}

void validatecommand()     							// function for checking if command has been sent and if it is valid by looking at the checksum
{
		if (commandflag==1)  						//if called when commandflag set at 1, validates received command and goes to command function
		{
			commands();
			commandflag=0;							//setting important command values at 0 so they can be changed again
			readpos=0;
		}
}

void commands()										//state machine type command function, depending on initial command byte,
{													//, executes different tasks then responds

	char commandtype = rxbuffer[0];
	char *numptr = &rxbuffer[1];
	val= atoi(numptr);

	switch (commandtype)
	{
	case 'D':														//attributes a dutycicle value as long as its less than 100%
		printf("\nchanging duty cycle to %d\n",val);
		if (val >100)
		{
			printf("dutycicle > 100");
		}
		else{
			dutycicle=val;
		}
		
									
	break;

	case 'F':
	if (val<250)
	{
		printf("\nfrequencia nao e possivel\n");
	}
	else
	{		//changes the frequency 
		printf("\nchanging frequency to %uHZ\n",val);

		NEWTOP=-1+16000000.0/val ;
	}
	break;

	case 'C':													// enables output compare interrupt which is set up to work with a fixed dutycicle value
		printf("\nnormal pwm\n");
		TIMSK1 = (1<<OCIE1A);
	break;

	case 'O':													//overwrites and enables overflow interrupt which changes the dutycicle automatically, generating
		printf("\nTrue wave function\n");						//a visible wave in the LED
		TIMSK1 = (1<<TOIE1);
	break;

	default:
	printf("\nno command type\n");
	break;

	}
	printf("OCR1a=%u\n",OCR1A);
	printf( "TOP = %u\n",NEWTOP);
}


/*void timer1config(){																//exercice 1

	
	TCCR1B = (1<<WGM13);			//generation mode=4 CTC with TOP at ICR1
	TCCR1A = (1<<WGM11);
	ICR1 = 0xF330;					//62256 - top number as to make this cycle last 1s
	TIMSK1 = (1<<OCIE1A);			//enabling interrupt of Output compare A match
	TCCR1B |= (1<<CS12);			//pre scaler N=256

}*/




void timer1pwmconfig()																//exercice 2
{
	
	TCCR1A = (1<<COM1A1)|(1<<WGM11);				//timer set up in mode 14 with TOP and Output values given by global variables
	ICR1 = NEWTOP;                       						
	OCR1A = (dutycicle/100.0)*NEWTOP;
	TIMSK1 = (1<<OCIE1A);							//only output compare interrup enabled, making it work with a fixed dutycicle
	TCCR1B = (1<<WGM13)|(1<<WGM12);
	TCCR1B |= (1<<CS10);


	
	/*TIMSK0 = (1<<TOIE0)|(1<<OCIE0A);
	TCCR0B |= (1<<CS02)|(1<<CS00);*/

}


ISR(TIMER1_COMPA_vect)								//interrupt to update values at every overflow(case for mode 14)
{ 
	ICR1 = NEWTOP;
	OCR1A = (dutycicle/100.0)*ICR1;
	
}

ISR(TIMER1_OVF_vect)								//Interrupt that automatically changes dutycicle and updates
{ 
	dutycicle +=0.5;
	if (dutycicle >100)
	{
		dutycicle=0;
	}
	
	ICR1 = NEWTOP;
	OCR1A = (dutycicle/100.0)*ICR1;

}

/*ISR(TIMER0_OVF_vect)             				//attempt at using another timer (timer 0) to change the dutycicle 
{
	dutycicle +=10;
	if (dutycicle>100)
	{
		dutycicle=0;
	}
	
}*/

static FILE mystdout = FDEV_SETUP_STREAM(uputc, NULL,_FDEV_SETUP_WRITE);


//uart receive isr
ISR(USART_RX_vect)
{

	uint8_t c = UDR0;
	
	switch (readflag)    					//condition set after '!' start bit to start registering command and data to be send
	{
		case 1:
			rxbuffer[readpos]=c;
			readpos++;
		break;

	default :
		break;
	}

	
	if (c=='!')  								// sets readflag at 1, making bytes being received in the next cycle to be stored in rxbuffer
		{
			readflag=1;
		}

	if (c=='\r')								//stop bit, sets command flag at 1, sets readflag at 0, calling validatecommand in main and stops storing incoming bytes to rxbuffer
	{
		commandflag=1;
		readflag=0;
	}

	loop_until_bit_is_set(UCSR0A, UDRE0);		/* wait until we can send a new byte */
	UDR0 = (uint8_t) c;

}




int main(void)
{
	/* tell stdio to use our stream as default */
	stdout=&mystdout;

	/* pin config */
	DDRB = (0<<PORTB5)|(1<<PORTB1);
	


	/* uart config */
	UCSR0A = (1 << U2X0); /* this sets U2X0 to 1 */   // doubles the rate of transmission 
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0); // Enable receiver, Enable Transmitter, Enable Receiver Complete Interrupt- call an interrup when done receiving
	UCSR0C = (3 << UCSZ00); /* this sets UCSZ00 to 3 */  //sets number of data bits( character size) to be used 3-> 011-> 8bit data

	/* baudrate setings (variable set by macros) */
	UBRR0H = (UBRRVAL) >> 8;
	UBRR0L = UBRRVAL;

	/* ADC cfg */
	sei();							  /* enable external interrupts */
	printf("ago\n");

	timer1pwmconfig();
	
	for (;;) 
	{
		validatecommand();     		// validatecommand always being called
		
	}
}

