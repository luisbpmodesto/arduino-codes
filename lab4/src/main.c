#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define UBRRVAL F_CPU/8/BAUD-1 /* calculates UBRRVAL for BAUD baudrate, BAUD must be defined */
#define buffer_size  128
char rxbuffer[buffer_size];
volatile int commandflag  = 0;
int readflag = 0;
int readpos =0;
int dcflag =0;
static int uputc(char,FILE*);
int val;
double dutycicle = 50.0;
int TOP=1024;
int COMP =50;
int ADCval;
int inv= 0;
int startflag=0;
int n =0;
int nmax=0;

void validatecommand();
void commands();
void timer1config();
void dutyciclechanger();
void ADCconfig();
void start(int freq);
static FILE mystdout = FDEV_SETUP_STREAM(uputc, NULL,_FDEV_SETUP_WRITE);


static int uputc(char c,FILE *stream)
/* uart writer function for libc stdio functions */
{
	if (c == '\n')
	uputc('\r',stream);
	loop_until_bit_is_set(UCSR0A, UDRE0);	/* wait until we can send a new byte */
	UDR0 = (uint8_t) c;
	return 0;
}
void validatecommand()
{   							// function for checking if command has been sent
	if (commandflag==1)  						//if called when commandflag set at 1, validates received command and goes to commands() function
	{
		commands();
		commandflag=0;							//setting important command values at 0 so they can be changed again
		readpos=0;
	}
}
void commands()										//state machine type command function, depending on initial command byte,
{													//, executes different tasks then responds

	char commandtype = rxbuffer[0];
	char *numptr = &rxbuffer[1];
	val= atoi(numptr);

	switch (commandtype)
	{
	case 'D':								//attributes a dutycicle value as long as its less than 100%

		if (val >100)
		{
			printf("dutycicle > 100");
		}
		else
		{
			dutycicle=val;
			printf("\n dutycicle changed to %d\n", val);
		}break;

	case 'F':							//changes frequency of timer1
	if (val <250)
	{
		printf("\n frequencia nao possivel \n");
	}
	else
	{
		printf("\nchanging frequency to %uHZ\n",val);
		TOP=-1+16000000.0/(val);
	}
	break;


	case 'C':							//changes frequency of timer0
	if ((val < 65)| (val > 7000))
	{
		printf("\n frequencia invalida\n");
	}
	else
	{
		OCR0A = -1 +16000000.0/(val);
	}
	
	case 'S':							//starts timer0 and activates ADC auto trigger on timer0 compare match with OC0A
	printf("\n command s\n");
		TCCR0B |=(1<<CS02)|(1<<CS00);
		ADCSRB |=(1<<ADTS1)|(1<<ADTS0);
		dcflag = 1;
		n=0;
		nmax=val;
	break;

	default:							//in case no valid command is given
	printf("\nno command type\n");
	break;
	}
}
void start(int freq)
{

if((freq>7812)|(freq<31))								//check if frequency within allowed values
	{
		printf("frequencia invalida\n"); 
	}
	else
	{
		printf("frequencia valida\n"); 
		OCR0A = F_CPU/(2*1024*freq)-1;
		COMP = OCR1B;
		OCR1B =0;
		startflag=1;
		TCCR0B |= (1<<CS02)|(1<<CS00);
	}
}
ISR(USART_RX_vect)						//USART mechanism to receive letters and store ones identified as command by '!'
{

	uint8_t c = UDR0;
	
	switch (readflag)    				//condition set after '!' start bit to start registering command and data to be send
	{
		case 1:
			rxbuffer[readpos]=c;
			readpos++;
		break;

	default :
		break;
	}

	
	if (c=='!')  								// sets readflag at 1, making bytes being received in the next cycle to be stored in rxbuffer
	{
		readflag=1;
	}

	if (c=='\r')								//stop bit, sets command flag at 1, sets readflag at 0, calling validatecommand in main and stops storing incoming bytes to rxbuffer
	{
		commandflag=1;
		readflag=0;
	}

	loop_until_bit_is_set(UCSR0A, UDRE0);		/* wait until we can send a new byte */
	UDR0 = (uint8_t) c;
}



void timer1pwmconfig()											
{
	TCCR1A |= (1<<COM1B1)|(1<<WGM11)|(1<<WGM10);				//timer set up in mode 15(Fast Pwm) with TOP and Output values given by global variables                   						
	TCCR1B |= (1<<WGM13)|(1<<WGM12);							//clear OC1B on compare match, set at BOTTOM
	//TIMSK1 = (1<<TOIE1);
	OCR1A = TOP;												//TOP=1024 giving timer1 frequency of 15kHz
	OCR1B = 500;
	TCCR1B |= (1<<CS10);										//prescaler of 1

}

void timer0config()
{
	TCCR0A |= (1<<WGM01)|(1<<COM0A0);							//timer 0 in CTC mode, toggling OC0A on compare match
	TIMSK0 =(1<<OCIE0A);
	TCNT0 = 0;
	OCR0A = 255;
}

void ADCconfig()												//simple ADC config with 125kHz frequency
{
	ADMUX = (1<<REFS0)|(1<<MUX0);								
	ADCSRA = (1<<ADEN)|(1<<ADIE);
	ADCSRA|= (1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
}

void autoADCconfig()											//ADC config for auto trigger, with start given by command S
{
	ADMUX = (1<<REFS0)|(1<<MUX0);								//ADC frequency 125kHz;
	ADCSRA = (1<<ADEN)|(1<<ADATE)|(1<<ADIE);
	ADCSRA|= (1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
}


void dutyciclechanger()											//function to change dutycycle by 1 increasing and decreasing
{														
	
	if (OCR1B>1020){inv=1; n++;}								//increments n every wave pique, to count periods
	if (OCR1B<10){inv=0;}
	
	if (inv==1)
	{
		OCR1B -= 1;	
	}

	else
	{
		OCR1B +=1;
	}
	_delay_ms(1.5);												//settling time for R=33000 ohm and 0.47 microFarads

}


ISR(ADC_vect)													//ADC ISR to stop ADC autotrigger after n_max periods
{
	printf("\n %d %d %u",n, ADC, OCR1B);
	
	if(n>nmax)
	{
	ADCSRA &= ~(1<<ADATE);
	}
}

ISR(TIMER0_COMPA_vect)
{
	
}


int main(void)
{
	/* tell stdio to use our stream as default */
	stdout=&mystdout;

	/* pin config */
	DDRB = (0<<PORTB5)|(1<<PORTB2);

	/* uart config */
UCSR0A = (1 << U2X0); /* this sets U2X0 to 1 */   // doubles the rate of transmission 
UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0); // Enable receiver, Enable Transmitter, Enable Receiver Complete Interrupt- call an interrup when done receiving
UCSR0C = (3 << UCSZ00); /* this sets UCSZ00 to 3 */  //sets number of data bits( character size) to be used 3-> 011-> 8bit data

/* baudrate setings (variable set by macros) */
UBRR0H = (UBRRVAL) >> 8;
UBRR0L = UBRRVAL;

	

	/* ADC cfg */
	sei();							  /* enable external interrupts */

	timer1pwmconfig();				 //coniguring all needed functions
	timer0config();
	autoADCconfig();
	printf("\n ola\n");
	for (;;) 
	{
		validatecommand();     		// validatecommand always being called
		if (dcflag==1)
		{
			dutyciclechanger();
		}
	}
}

