#include <avr/io.h>
#include <util/delay.h>
#include "read.h"

int main(void)
{
	/* I/O cfg */
	DDRB = (1 << DDB5); //Sets PORT5 of register B as output for LED
	PORTB = (1<<PORTB4);// Turns on pull up register 

//alinea 1
//	for (;;)  /*Loop infinito*/
//	{
//		if(bit_is_set(PINB,PINB4)){    // checks if PINB4 of PINB is set(1) returning true if yes or False if not
//			PORTB |= (1<<PORTB5);	   // If true makes or(|) statement with itself, with PORTB5 set to 1
//		} else{ 
//			PORTB &= ~(1<<PORTB5);		//If False makes and(&) statement with itself,with complement(~) of PORTB5 set to 1
//		}	
//	}




//alinea 4

	for(;;)
	{
		if(bit_is_clear(PINB,PINB4)) // checks if button is being pressed
		{
			PORTB ^= (1<<PORTB5); //makes an XOR operation with itself with B5 set at 1, toggling PINB5 everytime it runs
			_delay_ms(250); // allows the blinking to be seen
		}
		else
		{
			PORTB &= ~(1<<PORTB5); //makes an AND operation with itself with B5 set at complement of 1 (0), turning it off
		}
	}


//alinea 7a)
	


		main_loop();
}